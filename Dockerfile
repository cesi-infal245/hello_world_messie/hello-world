ARG NODEJS_VERSION=20-alpine

FROM node:${NODEJS_VERSION} as build-step

WORKDIR /app

COPY src ./src
COPY public ./public
COPY package.json .
COPY package-lock.json .
#Prepare for build
RUN npm install
#Build
RUN npm run build

#Prepare for deploy
FROM nginx:stable
#Copy nginx config
COPY nginx.conf /etc/nginx/nginx.conf
#Copy builded app
COPY --from=build-step /app/build /app/build
#Expose port
EXPOSE 80
#Run nginx
CMD ["nginx", "-g", "daemon off;"]